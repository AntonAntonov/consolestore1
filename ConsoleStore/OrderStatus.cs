﻿
namespace ConsoleStore
{
    public enum OrderStatus
    {
        New,
        CanceledByAdministrator,
        Paid,
        Sent,
        Completed,
        CanceledByUser,
        Received
    }
}
