﻿using System;
using System.Linq;

namespace ConsoleStore
{
    class Customer:User
    {
        public Customer(int userId, string firstName, string lastName, string login, string password, string email, string phoneNumber)
            : base(userId, firstName, lastName, login, password, email, phoneNumber)
        {

        }
    }
}
