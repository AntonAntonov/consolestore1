﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleStore
{
    static public class Moq
    {

        static List<Product> Products = new List<Product>();
        static List<User> Users = new List<User>();
        static List<Order> Orders = new List<Order>();

        static public List<User> getUsers()
        {
            return Users;
        }

        static public List<Product> getProducts()
        {
            return Products;
        }
        static public List<Order> getOrders()
        {
            return Orders;
        }

        public static void FillOrders()//Заполнение списка заказов
        {
            Dictionary<Product, int> productsToOrder = new Dictionary<Product, int>();

            productsToOrder.Add(Products.Single(c => c.ProductId == 1), 3);//
            productsToOrder.Add(Products.Single(c => c.ProductId == 4), 1);

            float totalPrice = 0;
            foreach (Product item in Products)
            {
                totalPrice += item.Price;
            }
            Orders.Add(new Order(Orders.Count + 1, 1, OrderStatus.Received, DateTime.Now, productsToOrder, totalPrice));
        }
        public static void FillUsers()//Заполнение списка пользователей
        {
            Users.Add(new Customer(Users.Count + 1, "Антон", "Антонов", "1", "1", "antonov@gmail.com", "0987364731"));
            Users.Add(new Customer(Users.Count + 1, "Сергей", "Антонов", "Antonov02", "qwerty2", "antonov@gmail.com", "0987364732"));
            Users.Add(new Admin(Users.Count + 1, "Дмитрий", "Антонов", "2", "2", "antonov@gmail.com", "0987364733"));
        }
        public static void FillProducts()//Заполнение списка продуктов
        {
            Products.Add(new Product(Products.Count + 1, "Программирование на C#","1" , 299, "Учебник по языку программирования"));
            Products.Add(new Product(Products.Count + 1, "Программирование на Python","1" , 199, "Учебник по языку программирования"));
            Products.Add(new Product(Products.Count + 1, "Программирование на C++","1" ,399, "Учебник по языку программирования"));
            Products.Add(new Product(Products.Count + 1, "MSSQL для чайников","2" ,499 ,"Учебник по работе с базами данных"));
        }

    }
}
