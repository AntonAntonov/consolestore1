﻿
namespace ConsoleStore
{
    public class Product
    {
        public int ProductId { get; set; }
       
        public string ProductName {get;set;}
        public float Price { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }

        public Product(int id,string name, string category, float price,string description)
        {
            ProductId = id;
            ProductName = name;
            Price = price;
            Category = category;
            Description = description;
        }

    }
}
