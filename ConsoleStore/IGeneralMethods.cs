﻿using System;

namespace ConsoleStore
{
    interface IGeneralMethods
    {
        public void ProductList()//Вывод списка товарор
        {
            foreach (Product item in Moq.getProducts())
            {
                Console.WriteLine(" "+item.ProductName + " " + item.Price + " " + item.Description);
            }

            Console.WriteLine("\n Для оформления заказа нужно авторизироваться \n");
            Console.WriteLine(" Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();
        }

        public void ProductSearch()//Поиск по названию продукта
        {
            Console.WriteLine(" Введите название продукта: ");
            string name = Console.ReadLine();

            foreach (Product item in Moq.getProducts())
            {
                if (item.ProductName.Contains(name))
                {
                    Console.WriteLine(item.ProductName + " " + item.Price + " " + item.Description);
                }
            }

            Console.WriteLine("\nДля оформления заказа нужно авторизироваться\n ");
            Console.WriteLine(" Нажмите любую кнопку для возвращения в главное меню\n ");
            Console.ReadLine();
            UserInterface.MainPage();
        }
    }
}
