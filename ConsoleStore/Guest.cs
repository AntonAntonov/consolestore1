﻿using System;

namespace ConsoleStore 
{
    class Guest : IGeneralMethods
    {
        public void Authorization()//Авторизация пользователя
        {
            Console.Write("\n Login: \n ");
            string login = Console.ReadLine();
            Console.Write(" Password: \n ");
            string password = Console.ReadLine();
            Console.WriteLine();

            foreach (User item in Moq.getUsers())
            {
                if (item.Login == login && item.Password == password)
                {
                    User.CheckAutorization = true; 
                    User.IdOfOnlineUser = item.UserId;
                    User.RoleOfOnlineUser = false;
                    Console.WriteLine(" Здравствуйте " + item.FirstName + " Вы успешно авторизировались ");
                    System.Threading.Thread.Sleep(1500);
                    UserInterface.MainPage();
                    return;
                }
            }

            Console.WriteLine(" Неверный логин или пароль!");
            Console.WriteLine(" 1)Попробовать еще раз");
            Console.WriteLine(" 2)Вернуться в главное меню");

            int button = int.Parse(Console.ReadLine());
            switch (button)
            {
                case 1:
                    Console.Clear();
                    Authorization();
                    break;
                case 2:
                    UserInterface.MainPage();
                    break;
                default:
                    Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                    Authorization();
                    break;
            }

        }

        public void Registration()//Сделать проверку на одинаковые Логины Эмейлы Номера телефонов
        {
            Console.WriteLine(" Введите имя");
            string firstName = Console.ReadLine();
            Console.WriteLine(" Введите фамилию");
            string lastName = Console.ReadLine();
            Console.WriteLine(" Введите Email");
            string Email = Console.ReadLine();
            Console.WriteLine(" Введите номер телефона");
            string phoneNumber = Console.ReadLine();
            Console.WriteLine(" Login:");
            string login = Console.ReadLine();
            Console.WriteLine(" Password");
            string password = Console.ReadLine();

            Moq.getUsers().Add(new Customer(Moq.getUsers().Count + 1, firstName, lastName, login, password, Email, phoneNumber));

            UserInterface.MainPage();

        }
    }
}
