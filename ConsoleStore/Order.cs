﻿using System;
using System.Collections.Generic;


namespace ConsoleStore
{
    public class Order
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
        //1 - не оплачено, 2 - оплачено, 3 - получено  
        public OrderStatus Status { get; set; } // Возможно нужно создать перечисление
        public DateTime Date { get; set; }

        readonly Dictionary<Product, int> ProductsInOrder;

        public Dictionary<Product, int> getProductsInOrder()
        {
            return ProductsInOrder;
        }

        public float TotalPrice { get; set; }

        public Order(int orderId, int userId, OrderStatus status, DateTime date, Dictionary<Product, int> products,float totalPrice )
        {
            OrderId = orderId;
            UserId = userId;
            Status = status;
            Date = date;
            ProductsInOrder = products;
            TotalPrice = totalPrice;
        }
    }
}
