﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace ConsoleStore
{
    public enum Page//замента свич кейс в будущем
    {
        Autorization = 1,
        Registration,
        ProductList,
        ProductSearch,
        AddToCart,
        Cart,
        ShowMyOrders,
        PersonalArea,
        Exit,
        UserList,
        OrderList,
        ChangeUserData,
        AddNewProduct,
    }

    static public class UserInterface
    {

        static public void MainPage()//Главная страница с выбором функций
        {
            Console.Clear();
            Dictionary<Page, Action> dictionary = new Dictionary<Page, Action>();
            Console.WriteLine("    ДОБРО ПОЖАЛОВАТЬ В ИНТЕРНЕТ-МАГАЗИН \"КНИЖНАЯ ЛАВКА\"  \n");

            Page button = 0;

            if (!User.CheckAutorization)
            {
                Guest guest = new Guest();

                Console.WriteLine(" Выберите действие: ");
                Console.WriteLine(" 1) Авторизация ");
                Console.WriteLine(" 2) Регистрация ");
                Console.WriteLine(" 3) Список товаров ");
                Console.Write(" 4) Поиск товара по названию  \n ");

                dictionary.Add(Page.Autorization, new Action(guest.Authorization));
                dictionary.Add(Page.Registration, new Action(guest.Registration));
                dictionary.Add(Page.ProductList, new Action(((IGeneralMethods)guest).ProductList));
                dictionary.Add(Page.ProductSearch, new Action(((IGeneralMethods)guest).ProductSearch));
                dictionary.Add(default, MainPage);

                int temp = 0;
                try
                {
                    temp = int.Parse(Console.ReadLine());
                }
                catch (System.FormatException)
                {
                    Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    dictionary[default]();
                }

                button = (Page)temp;

                Console.Clear();
                if (dictionary.ContainsKey(button))
                    dictionary[button]();
                else
                {
                    Console.WriteLine("Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    dictionary[default]();
                }
            }
            else if (User.CheckAutorization)//Если пользователь авторизирован
            {
                if(Moq.getUsers().Single(c=>c.UserId == User.IdOfOnlineUser) is Customer)//если customer
                {
                    Customer customer = (Customer)Moq.getUsers().Single(c => c.UserId == User.IdOfOnlineUser);

                    Console.WriteLine(" Выберите действие: ");
                    Console.WriteLine(" 3) Список товаров ");
                    Console.WriteLine(" 4) Поиск товара по названию ");
                    Console.WriteLine(" 5) Добавить товар в корзину");
                    Console.WriteLine(" 6) Корзина");
                    Console.WriteLine(" 7) Список заказов");
                    Console.WriteLine(" 8) Личный кабинет");
                    Console.Write(" 9) Выйти с учётной записи \n ");

                    dictionary.Add(Page.ProductList, new Action(((IGeneralMethods)customer).ProductList));
                    dictionary.Add(Page.ProductSearch, new Action(((IGeneralMethods)customer).ProductSearch));
                    dictionary.Add(Page.AddToCart, new Action(customer.AddToCart));
                    dictionary.Add(Page.Cart, new Action(customer.Cart));
                    dictionary.Add(Page.ShowMyOrders, new Action(customer.MyOrders));
                    dictionary.Add(Page.PersonalArea, new Action(customer.PersonalArea));
                    dictionary.Add(Page.Exit, new Action(customer.Exit));
                    dictionary.Add(default, MainPage);

                   

                    try { button = (Page)int.Parse(Console.ReadLine());}
                    catch (System.FormatException)
                    {
                        Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                        System.Threading.Thread.Sleep(1500);
                        dictionary[default]();
                    }

                    Console.Clear();
                    if (dictionary.ContainsKey(button))
                        dictionary[button]();
                    else
                    {
                        Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                        System.Threading.Thread.Sleep(1500);
                        dictionary[default]();
                    }
                }
                else // если админ
                {
                    Admin admin = (Admin)Moq.getUsers().Single(c => c.UserId == User.IdOfOnlineUser);

                    Console.WriteLine(" Выберите действие: ");
                    Console.WriteLine(" 3) Список товаров ");
                    Console.WriteLine(" 4) Поиск товара по названию ");
                    Console.WriteLine(" 5) Добавить товар в корзину");
                    Console.WriteLine(" 6) Корзина");
                    Console.WriteLine(" 7) Список заказов");
                    Console.WriteLine(" 8) Личный кабинет");
                    Console.WriteLine(" 9) Выйти с учётной записи");
                    Console.WriteLine(" 10) Список пользователей");
                    Console.WriteLine(" 11) Список всех заказов");
                    Console.WriteLine(" 12) Изменить данные пользователя");
                    Console.Write(" 13) Добавить новый товар /n ");
                    
                    dictionary.Add(Page.ProductList, new Action(((IGeneralMethods)admin).ProductList));
                    dictionary.Add(Page.ProductSearch, new Action(((IGeneralMethods)admin).ProductSearch));
                    dictionary.Add(Page.AddToCart, new Action(admin.AddToCart));
                    dictionary.Add(Page.Cart, new Action(admin.Cart));
                    dictionary.Add(Page.ShowMyOrders, new Action(admin.MyOrders));
                    dictionary.Add(Page.PersonalArea, new Action(admin.PersonalArea));
                    dictionary.Add(Page.Exit, new Action(admin.Exit));
                    dictionary.Add(Page.UserList, new Action(admin.UserList));
                    dictionary.Add(Page.OrderList, new Action(admin.OrderList));
                    dictionary.Add(Page.ChangeUserData, new Action(admin.ChangeUserData));
                    dictionary.Add(Page.AddNewProduct, new Action(admin.AddNewProduct));
                    dictionary.Add(default, MainPage);

                    int temp = 0;
                    try
                    {
                        temp = int.Parse(Console.ReadLine());
                    }
                    catch (System.FormatException)
                    {
                        Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                        System.Threading.Thread.Sleep(1500);
                        dictionary[default]();
                    }

                    button = (Page)temp;

                    Console.Clear();
                    if (dictionary.ContainsKey(button))
                        dictionary[button]();
                    else
                    {
                        Console.WriteLine("Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                        System.Threading.Thread.Sleep(1500);
                        dictionary[default]();
                    }
                }
            }
        }
    }
    static class Program
    {
        static void Main(string[] args)
        {

            Moq.FillUsers();
            Moq.FillProducts();
            Moq.FillOrders();

            UserInterface.MainPage();
        }
    }
}
