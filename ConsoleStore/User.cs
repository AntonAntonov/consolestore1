﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleStore
{
    abstract public class User:IGeneralMethods
    {

        public static bool CheckAutorization { get; set; } = false;//Проверка,авторизирован ли юзер
        public static int IdOfOnlineUser { get; set; }// АйДи юзера
        public static bool RoleOfOnlineUser { get; set; }// Роль юзера (Админ , кастомер) //Возможно сделать перечисление
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        protected User(int userId, string firstName, string lastName, string login, string password, string email, string phoneNumber)
        {
            UserId = userId;
            FirstName = firstName;
            LastName = lastName;
            Login = login;
            Password = password;
            Email = email;
            PhoneNumber = phoneNumber;
            MyCart = new Dictionary<Product, int>();
        }

        readonly Dictionary<Product, int> MyCart; 

        public void PersonalArea()
        {
            User temp = Moq.getUsers().Single(c => c.UserId == IdOfOnlineUser);
            Console.WriteLine(" Имя: "+ temp.FirstName);
            Console.WriteLine(" Фамилия: " + temp.LastName);
            Console.WriteLine(" Логин: " + temp.Login);
            Console.WriteLine(" Пароль: " + temp.Password);
            Console.WriteLine(" Электронная почта: " + temp.Email);
            Console.WriteLine(" Номер телефона: " + temp.PhoneNumber);
            Console.WriteLine(" ID: " + temp.UserId);

            ChangeMyData();
        }
        public void ProductList()//Вывод списка товарор
        {
            foreach (Product item in Moq.getProducts())
            {
                Console.WriteLine(" "+item.ProductName + " " + item.Price + " " + item.Description);
            }

            Console.WriteLine(" Выберите действие: ");
            Console.WriteLine(" 1) Вернуться в главное меню");
            Console.WriteLine(" 2) Оформить заказ");
            int button = int.Parse(Console.ReadLine());

            switch (button)
            {
                case 1:
                    UserInterface.MainPage();
                    break;
                case 2:
                    AddToCart();
                    break;

                default:
                    Console.WriteLine(" Вы нажали неправильную кнопкуй Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                    ProductList();
                    break;
            }
        }

        public void ProductSearch()//Поиск по названию продукта
        {
            Console.WriteLine(" Введите название товара: ");
            string name = Console.ReadLine();

            foreach (Product item in Moq.getProducts())
            {
                if (item.ProductName.Contains(name))
                {
                    Console.WriteLine(" "+item.ProductName + " " + item.Price + " " + item.Description);
                }
            }

            Console.WriteLine(" Выберите действие: ");
            Console.WriteLine(" 1) Вернуться в главное меню");
            Console.WriteLine(" 2) Оформить заказ");
            int button = int.Parse(Console.ReadLine());

            switch (button)
            {
                case 1:
                    UserInterface.MainPage();
                    break;
                case 2:
                    break;
                default:
                    Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                    ProductSearch();
                    break;
            }
        }
        public void ChangeMyData()
        {
            Console.WriteLine(" ИЗМЕНЕНИЕ ЛИЧНЫХ ДАННЫХ \n");
            Console.WriteLine(" Выберите, что хотите изменить: ");
            Console.WriteLine(" 1) Имя");
            Console.WriteLine(" 2) Фамилия");
            Console.WriteLine(" 3) Пароль");
            Console.WriteLine(" 4) Электронная почта");
            Console.WriteLine(" 5) Номер телефона");
            Console.Write(" 6) Вернуться в главное меню \n ");

            int button = int.Parse(Console.ReadLine());

            switch (button)
            {
                case 1:
                    Console.Write(" Введите новое имя \n ");
                    FirstName = Console.ReadLine();
                break;
                case 2:
                    Console.Write(" Введите новую фамилию \n ");
                    LastName = Console.ReadLine();
                    break;
                case 3:
                    Console.Write(" Введите новый пароль \n ");
                    Password = Console.ReadLine();
                    break;
                case 4:
                    Console.Write(" Введите новую электронную почту \n ");
                    Email = Console.ReadLine();
                    break;
                case 5:
                    Console.Write(" Введите новый номер телефона \n ");
                    PhoneNumber = Console.ReadLine();
                    break;
                case 6:
                    UserInterface.MainPage();
                    break;
                default:
                    Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка! \n ");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                    PersonalArea();
                    break;
            }

            Console.WriteLine(" Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();
        }
        public void Cart()//Вывод корзины 
        {
            foreach (KeyValuePair<Product, int> item in MyCart)
            {
                Console.WriteLine(item.Key.ProductName + "\t" + item.Key.Price + "\t" + item.Key.Description + "\t|" + item.Value + "\n");
            }
            if(MyCart.Count!=0)
            {
                Console.WriteLine(" Выберите действие");
                Console.WriteLine(" 1)Оплатить");
                Console.WriteLine(" 2)Отменить");
                Console.WriteLine(" 3)Вернуться в главное меню");

                int button = Convert.ToInt32(Console.ReadLine());
                switch (button)
                {
                    case 1:
                        Console.Clear();
                        Moq.getOrders().Last().Status = OrderStatus.Paid;//Оплатить заказ
                        MyCart.Clear();
                        UserInterface.MainPage();
                        break;
                    case 2:
                        Console.Clear();
                        Moq.getOrders().Last().Status = OrderStatus.CanceledByUser;//Отменить заказ
                        MyCart.Clear();
                        UserInterface.MainPage();                      
                        break;
                    case 3:
                        Console.Clear();
                        UserInterface.MainPage();
                        break;
                    default:
                        Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                        System.Threading.Thread.Sleep(1500);
                        Console.Clear();
                        Cart();
                        break;
                }
            }
            else
            {
                Console.WriteLine(" Корзина пуста!");
            }

            Console.WriteLine(" Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();

        }

        public void ChangeOrderStatus()
        {
            Console.Write(" Введите Номер вашего заказа, статус которого хотите изменить на Получено \n ");
            int id = Convert.ToInt32(Console.ReadLine());
            Moq.getOrders().Single(c => c.OrderId == id).Status = OrderStatus.Received;

            Console.WriteLine(" Статус изменён! Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();
        }

        public void MyOrders()//Вывод всех заказов авторизованного пользователя 
        {
            int count=0;
            foreach (Order item in Moq.getOrders())
            {
                if (item.UserId == IdOfOnlineUser)
                {
                    Console.WriteLine(" Id Заказа : "+item.OrderId +" | Дата и время заказа: "+ item.Date + " | Статус: " + item.Status );
                    foreach (KeyValuePair<Product, int> item2 in item.getProductsInOrder())
                    {
                        count++;
                        Console.WriteLine(" "+item2.Key.ProductId + " " + item2.Key.ProductName + " " + item2.Key.Price + " | " + item2.Value);
                    }
                }
            }

            if (count == 0)
            {
                Console.WriteLine(" Список заказов пуст! \n");
            }
            Console.WriteLine("\n\n Выберите действие ");
            Console.WriteLine(" 1) Изменить статус заказа на получено");
            Console.Write(" 2) Вернуться в главное меню \n ");
            int button = Convert.ToInt32(Console.ReadLine());

            switch (button)
            {
                case 1:
                    ChangeOrderStatus();
                    break;
                case 2:
                    Console.Clear();
                    UserInterface.MainPage();
                    break;
                default:
                    Console.WriteLine(" Вы нажали неправильную кнопку. Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                    MyOrders();
                    break;
            }

            Console.WriteLine(" Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();

        }

        public void Exit()//Перезапуск главного меню, где проходит проверка какое меню запускать
        {
            CheckAutorization = false;
            UserInterface.MainPage();
        }

        public void AddToCart()//Добавление товарорв в корзину // Формрование заказа, в корзине же можно будет оплатить или отменить заказ, в списке заказов можно сменить на получено 
        {
            foreach (Product item in Moq.getProducts())
            {
                Console.WriteLine(" "+item.ProductId + ")  " + item.ProductName + " " + item.Price + " " + item.Description);
            }

            int button = 1;
            int number;
            int count;

            Dictionary<Product, int> productsToCart = new Dictionary<Product, int>();//Возможно в юзере стоит сделать Dictionary<Product, int> MyCart для сохранения корзины при выходе

            while (button != 2)
            {
                Console.WriteLine(" Выберите номер и колличество товара");
                Console.Write(" Номер: ");
                number = Convert.ToInt32(Console.ReadLine());
                Console.Write(" Колличество: ");
                count = Convert.ToInt32(Console.ReadLine());

                productsToCart.Add(Moq.getProducts().Single(c => c.ProductId == number), count);//


                Console.WriteLine(" 1) Продолжить");
                Console.WriteLine(" 2) Закончить");
                button = Convert.ToInt32(Console.ReadLine());
                switch(button)
                {
                    case 1:
                    break;
                    case 2:
                        foreach (KeyValuePair<Product, int> item in productsToCart)
                            MyCart.Add(item.Key,item.Value);
                        break;
                    default:
                        button = 1;
                        Console.WriteLine(" Вы нажали неправильную кнопкуй Выберите кнопку из списка!");
                        System.Threading.Thread.Sleep(1500);
                        break;

                }

            }

            float totalPrice = 0;
            foreach (KeyValuePair<Product, int> item in productsToCart)
            {
                totalPrice += item.Key.Price;
            }

           
            Moq.getOrders().Add(new Order(Moq.getOrders().Count + 1, IdOfOnlineUser, OrderStatus.New, DateTime.Now, productsToCart, totalPrice));//Установка статус новый!
            
            Cart();
        }

    }
}
