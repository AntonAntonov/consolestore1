﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleStore
{
    public class Admin:User
    {
        public Admin(int userId, string firstName, string lastName, string login, string password, string email, string phoneNumber) 
            : base(userId, firstName, lastName, login, password, email, phoneNumber)
        {

        }

        public void AddNewProduct()
        {
            Console.WriteLine("Введите название товара");
            string name = Console.ReadLine();
            Console.WriteLine("Введите категорию товара");
            string category = Console.ReadLine();
            Console.WriteLine("Введиите цену товара");
            int price = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите описание товара");
            string description = Console.ReadLine();

            Moq.getProducts().Add(new Product(Moq.getProducts().Count + 1, name , category, price, description));

            Console.WriteLine("Товар успешно добавлен");

            Console.WriteLine("Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();
        }

        public void ChangeOrderStatus()
        {
            Console.WriteLine("Введите Id заказа статус которого требуется изменить");
            int id = int.Parse(Console.ReadLine());
            Order order = Moq.getOrders().Single(c => c.OrderId == id);
            Console.Clear();
            Console.WriteLine("Id Заказа : " + order.OrderId + " | Дата и время заказа: " + order.Date + " | Статус: " + order.Status);
            foreach (KeyValuePair<Product, int> item2 in order.getProductsInOrder())
            {
                Console.WriteLine(item2.Key.ProductId + " " + item2.Key.ProductName + " " + item2.Key.Price + " | " + item2.Value);
            }

            Console.WriteLine("Какой статус требуется указать: ");
            Console.WriteLine("1) Отменено администратором");
            Console.WriteLine("2) Оплачено");
            Console.WriteLine("3) Отправленно ");
            Console.WriteLine("4) Завершено ");

            int button = int.Parse(Console.ReadLine());

            switch (button)
            {
                case 1:
                    order.Status = OrderStatus.CanceledByAdministrator;
                    break;
                case 2:
                    order.Status = OrderStatus.Paid;
                    break;
                case 3:
                    order.Status = OrderStatus.Sent;
                    break;
                case 4:
                    order.Status = OrderStatus.Completed;
                    break;
                case 5:
                    UserInterface.MainPage();
                    break;
                default:
                    Console.WriteLine("Вы нажали неправильную кнопкуй Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                    OrderList();
                    break;
            }

            Console.WriteLine("Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();

        }

        public void OrderList()
        {
            int count = 0;
            foreach (Order item in Moq.getOrders())
            {
                    Console.WriteLine("Id Заказа : " + item.OrderId + " | Дата и время заказа: " + item.Date + " | Статус: " + item.Status);
                    foreach (KeyValuePair<Product, int> item2 in item.getProductsInOrder())
                    {
                        count++;
                        Console.WriteLine(item2.Key.ProductId + " " + item2.Key.ProductName + " " + item2.Key.Price + " | " + item2.Value);
                    } 
            }

            if (count == 0)
            {
                Console.WriteLine("Список заказов пуст! \n");
            }

            Console.WriteLine("Выберите действие");

            Console.WriteLine("1) Вернуться в главное меню");
            Console.WriteLine("2) Изменить статус заказа");
            int button;
            button = Convert.ToInt32(Console.ReadLine());
            switch (button)
            {
                case 1:
                    Console.Clear();
                    UserInterface.MainPage();
                    break;
                case 2:
                    ChangeOrderStatus();
                    break;
                default:
                    Console.WriteLine("Вы нажали неправильную кнопкуй Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                    OrderList();
                    break;
            }

            Console.WriteLine("Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();
        }
        public void UserList()// Вывод списка пользователей
        {
            foreach (User item in Moq.getUsers())
            {
                Console.WriteLine(item.UserId + " " + item.FirstName + " " + item.LastName + " " + item.Login + item.Email + item.PhoneNumber);
            }

            Console.WriteLine("Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();

        }
        public void ChangeUserData()
        {
            Console.WriteLine("Введите Id Пользователя");
            int id = int.Parse(Console.ReadLine());

            Console.WriteLine("ИЗМЕНЕНИЕ ЛИЧНЫХ ДАННЫХ \n");
            Console.WriteLine("Выберите, что хотите изменить: ");
            Console.WriteLine("1) Имя");
            Console.WriteLine("2) Фамилия");
            Console.WriteLine("3) Логин");
            Console.WriteLine("4) Пароль");
            Console.WriteLine("5) Электронная почта");
            Console.WriteLine("6) Номер телефона");
            Console.WriteLine("7) Вернуться в главное меню");

            int button = int.Parse(Console.ReadLine());

            switch (button)
            {
                case 1:
                    Console.WriteLine("Введите новое имя");
                    Moq.getUsers().Single(c => c.UserId == id).FirstName = Console.ReadLine();
                    break;
                case 2:
                    Console.WriteLine("Введите новую фамилию");
                    Moq.getUsers().Single(c => c.UserId == id).LastName = Console.ReadLine();
                    break;
                case 3:
                    Console.WriteLine("Введите новый логин");
                    Moq.getUsers().Single(c => c.UserId == id).Login = Console.ReadLine();
                    break;
                case 4:
                    Console.WriteLine("Введите новый пароль");
                    Moq.getUsers().Single(c => c.UserId == id).Password = Console.ReadLine();
                    break;
                case 5:
                    Console.WriteLine("Введите новую электронную почту");
                    Moq.getUsers().Single(c => c.UserId == id).Email = Console.ReadLine();
                    break;
                case 6:
                    Console.WriteLine("Введите новый номер телефона");
                    Moq.getUsers().Single(c => c.UserId == id).PhoneNumber = Console.ReadLine();
                    break;
                case 7:
                    UserInterface.MainPage();
                    break;
                    Console.WriteLine("Вы нажали неправильную кнопкуй Выберите кнопку из списка!");
                    System.Threading.Thread.Sleep(1500);
                    Console.Clear();
                    UserList();
                    break;
            }

            Console.WriteLine("Нажмите любую кнопку для возвращения в главное меню");
            Console.ReadLine();
            UserInterface.MainPage();
        }

    }
}
